
public class Client {
	String name, email;
	int cedula;
	
	public Client(String name, String email, int cedula) {
		setName(name);
		setEmail(email);
		setCedula(cedula);
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public int getCedula() {
		return cedula;
	}
	public void setCedula(int cedula) {
		this.cedula = cedula;
	}

}
